//load all required scripts
;(function(context) {
	
	var gmap;
	var tests;
	var debounce;
	var preventOverScroll;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		preventOverScroll = context.preventOverScroll;
		tests = context.tests;
		imageLoader = context.imageLoader;
		gmap = context.gmap;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		preventOverScroll = require('./scripts/preventOverScroll.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		gmap = require('./scripts/gmap.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();
	preventOverScroll($('div.nav')[0]);	

	//all generic faders/heros
	$('.fader').each(function() {
		var 
			
			slickEl,
			el = $(this),
			methods = {
				
				getElementWithSrcData: function(el) {
					return el.data('src') !== undefined ? el : el.find('.fader-item-bg').filter(function() { return $(this).data('src') !== undefined });
				},
				
				setImageOnElements: function(els,source) {
					els.each(function() {
						$(this)
							.css({backgroundImage: 'url('+source+')' })
							.addClass('loaded');
					});
				},
				
				loadImageForElementAtIndex: function(i) {
					var 
						self = this;
						element = $('.fader-item',el).eq(i),
						sourceElement = this.getElementWithSrcData(element),
						rawSource = sourceElement.data('src'),
						source = imageLoader.getAppropriateSource(rawSource),
						allElements = sourceElement.add(self.getElementWithSrcData(element.siblings()).filter(function() { 
							return $(this).data('src') === rawSource; 
						}));
					
						element.addClass('loading');
						
						if(!source) {
							element.addClass('loaded');
						}
						
						if(imageLoader.hasSourceLoaded(source)) {
							this.setImageOnElements(allElements,source);
							return;
						}
						
						imageLoader
							.loadSource(source)
							.then(function() {
								self.setImageOnElements(allElements,source);
								element.addClass('loaded');
							});
					
				}

			};
			
		el.slick({
			dots:true,
			appendDots:$('.fader-nav',el.parent()),
			appendArrows:$('.fader-controls',el.parent()),
			prevArrow: '<button class="prev"/>',
			nextArrow: '<button class="next"/>',
			draggable:false,
			swipe:true,
			touchMove:true,
			autoplay:true,
			autoplaySpeed: 5000,
			pauseOnHover: false,
			fade:!tests.touch()
		});
		
		el.on('beforeChange',function(slick,e,i) {
			methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));
		});
		
		methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide'));
		
		$(window).on('resize',function() {
			d.requestProcess(function() { methods.loadImageForElementAtIndex(el.slick('slickCurrentSlide')); }); 
		})

	});

	//Map-Status-Photos section
	(function() {
		
		var methods = {
			
			generateRandomId: function() {
				return Math.random().toString(36).substr(2);
			},
			
			onTabChanged: function(el) {
				if(el.hasClass('msp-map-tab') && !el.data('mspbuilt')) {
					this.buildMap(el);
					return;
				}
				
				if(el.hasClass('msp-photos-tab') && !el.data('mspbuilt')) {
					this.buildPhotoSwipers(el);
				}
			},
			
			loadLargeImage: function(i,el) {
				var 
					items = el.find('.swipe-item'),
					slides = items.filter(function() { return !$(this).hasClass('slick-cloned'); }),
					sourceElement = slides.eq(i);
					rawSource = sourceElement.data('src'),
					source = imageLoader.getAppropriateSource(rawSource),
					allElements = items.filter(function(i,el) {
						return $(el).data('src') === rawSource;
					});
					
					imageLoader
						.loadSource(source)
						.then(function() {
							allElements
								.addClass('loaded')
								.children('span')
								.css({backgroundImage:'url('+source+')'})
								
						});
			},
			
			buildPhotoSwipers: function(el) {
				el.data('mspbuilt',true);
				
				var 
					self = this,
					randId = this.generateRandomId(),
					lgRandId = 'lgImages_'+randId,
					smRandId = 'smImages_'+randId,
					lgImages = $('.swiper-wrapper-lg-images',el),
					smImages = $('.swiper-wrapper-sm-images',el);
					
					lgImages.find('div.swiper').attr('id',lgRandId);
					smImages.find('div.swiper').attr('id',smRandId);
				
				//load a large image on demand
				lgImages.on('afterChange init',function(e,slick,i) { 
					self.loadLargeImage(slick.slickCurrentSlide(),lgImages); 
				});
					
				//generate a random ID
				//lgImages.attr('id',randId);
				swiper.buildSwiper(
					lgImages,
					{
						dots: true,
						asNavFor: '#'+smRandId,
						arrows: false
					}
				);
				
				
				swiper.buildSwiper(
					smImages,
					{
						centerMode: true,
						focusOnSelect: true,
						asNavFor:'#'+lgRandId,
						slidesToShow: 3
					}
				);
				
			},
			
			buildMap: function(el) {
				el.data('mspbuilt',true);
				gmap.buildMap(el.find('.msp-map').children('div'),{
					zoomControlOptions: {
						style: 1, //google.maps.ZoomControlStyle.SMALL,
						position: 3, //google.maps.ControlPosition.TOP_RIGHT
					},
					draggable: !tests.touch()
				});
			}
			
		};
		
		$('.msp-tabs')
			.on('tabChanged',function(e,el) {
				methods.onTabChanged(el);
			});
		
	}());

}(typeof ns !== 'undefined' ? window[ns] : undefined));