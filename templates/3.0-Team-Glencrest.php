<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item dark-bg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hgroup">
							<h1 class="hgroup-title">Team</h1>
						</div><!-- .hgroup -->
					
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet tellus elit. Ut eget mollis lacus. Donec molestie nibh eu nulla tempor porta. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices condimentum pellentesque. 
						</p>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content">
					
					<div class="grid">
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-1.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-2.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-3.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-4.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-5.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2">
							<div class="item center">
							
								<a href="#" class="tm-wrap bounce sm">
									<div class="tm lazybg img" data-src="../assets/images/temp/tm-6.jpg">&nbsp;</div>
								</a>
								
								<span class="tm-name">Team Member</span>
								<small class="tm-title">Job Title</small>
								
								<a href="#" class="darkblue button">View</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
					
				</div><!-- .content -->
				
			</div><!-- .main-body -->		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>