			<footer class="dark-bg">
				<div class="sw">				
					<div class="footer-wrap">
				
						<div class="footer-nav">
							<ul>
								<li><a href="#">Team</a></li>
								<li><a href="#">Benefits</a></li>
								<li><a href="#">Property Map</a></li>
								<li><a href="#">Phases</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</div><!-- .footer-nav -->
					
						<?php include('i-social.php'); ?>
					
					</div><!-- .footer-wrap -->
				</div><!-- .sw -->
				
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">DEW Corp</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<address>57 Harvey Road | St. John's, NL | A1C 5W1, Canada</address>
					</div><!-- .,sw -->
				</div><!-- .copyright -->

				
			</footer>
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/glencrest'
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>