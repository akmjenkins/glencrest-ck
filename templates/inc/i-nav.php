<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw">
		
		<nav>
			<ul>
				<li><a href="#">Team</a></li>
				<li><a href="#">Benefits</a></li>
				<li><a href="#">Property Map</a></li>
				<li><a href="#">Phases</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</nav>
	
		<div class="nav-top">
			
			<div class="nav-top-items">
				<span>1 (888) 143-2121</span>
			</div><!-- .nav-top-items -->
			
			<form class="single-form global-search-form" action="/">
				<div class="fieldset">
					<input type="text" name="s" placeholder="Search">
					<button class="t-fa fa-search">&nbsp;</button>		
					<button type="button" class="t-fa fa-close toggle-search search-close">&nbsp;</button>
				</div><!-- .fieldset -->
			</form><!-- .single-form -->
			
		</div><!-- .nav-top -->
		
	</div><!-- .sw -->
	
</div><!-- .nav -->