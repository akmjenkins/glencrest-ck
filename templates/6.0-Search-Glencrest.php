<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item dark-bg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hgroup">
							<h1 class="hgroup-title">Search Results</h1>
						</div><!-- .hgroup -->
					
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet tellus elit. Ut eget mollis lacus. Donec molestie nibh eu nulla tempor porta. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices condimentum pellentesque. 
						</p>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>

		<div class="filter-section">
			<div class="sw">
			
				<div class="filter-bar">

					<div class="filter-bar-left">
					
						<span class="filter-bar-title">Search results for "Query"</span>
						<span class="filter-bar-subtitle">Suspendisse magna lacusSuspendisse magna lacus</span>
						
					</div><!-- .filter-bar-left -->

					<div class="filter-bar-meta">
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
						
				</div><!-- .filter-bar -->
				
			</div><!-- .sw -->
			
			<div class="filter-content">
			
				<div class="sw">
				
					<div class="grid eqh search-grid">
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col">
							<div class="item">
							
								<div class="hgroup">
									<span class="hgroup-title">Page Title</span>
								</div><!-- .hgroup -->
								
								<p>
									Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
								</p>
							
								<a href="#" class="button outline">Read More</a>
							
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				
				</div><!-- .sw -->
				
			</div><!-- .filter-content -->
		</div><!-- .filter-section -->

	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>