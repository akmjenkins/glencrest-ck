<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item dark-bg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hgroup">
							<h1 class="hgroup-title">Contact</h1>
						</div><!-- .hgroup -->
					
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet tellus elit. Ut eget mollis lacus. Donec molestie nibh eu nulla tempor porta. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices condimentum pellentesque. 
						</p>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
				<h2>Location</h2>
		
				<div class="grid contact-grid">

					<div class="col">
						<div class="item">
							
							<div class="grid">
							
								<div class="col col-2">
									<div class="item">
										
										<address>
											57 Harvey Road <br />
											St. John's, NL <br />
											A1C 5W1, Canada
										</address>
										
										<br />
										
										<div class="rows">
										
											<div class="row">
												<span class="l">Phone:</span>
												<span class="r">1 709 754 0555</span>
											</div><!-- .row -->
											
											<div class="row">
												<span class="l">Fax:</span>
												<span class="r">1 709 754 0555</span>
											</div><!-- .row -->
											
										</div><!-- .rows -->
										
									</div><!-- .item -->
								</div><!-- .col -->
								
								<div class="col col-2">
									<div class="item">
										
										<strong class="uc">Office Hours</strong>
										
										<span class="block">Monday 8:00am - 7:30pm</span>
										<span class="block">Tuesday 8:00am - 5:00pm</span>
										<span class="block">Wednesday 8:00am - 5:00pm</span>
										<span class="block">Thursday 8:00am - 5:00pm</span>
										<span class="block">Friday 8:00am - 1:00pm</span>
										
									</div><!-- .item -->
								</div><!-- .col -->
							
								<div class="col col-1">
									<div class="item">
									
										<div class="embedded-gmap-wrap">
											<div class="embedded-gmap">
												<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1346.074908850923!2d-52.71411039999999!3d47.56487340000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca3bcf6218969%3A0x30b59d2714d7bd39!2s82+Harvey+Rd%2C+St.+John&#39;s%2C+NL+A1C+2G1!5e0!3m2!1sen!2sca!4v1423853227467" frameborder="0" style="border:0"></iframe>
											</div><!-- .embedded-gmap -->
										</div><!-- .embedded-gmap-wrap -->
									
									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->
						
						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col">
						<div class="item">
						
							<p>Fill out the form below to contact us:</p>
							
							<br />
						
							<form action="/" class="body-form contact-form">
								<div class="fieldset">
								
									<span class="field-wrap t-fa fa-user">
										<input type="text" name="name" placeholder="Full Name">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa fa-envelope">
										<input type="text" name="email" placeholder="E-mail">
									</span><!-- .field-wrap -->
									
									<span class="field-wrap t-fa fa-comment t-fa-top">
										<textarea name="message" placeholder="Message"></textarea>
									</span>
									
									<button class="button">Submit</button>
								
								</div><!-- .fieldset -->
							</form><!-- .body-form -->
							
						</div><!-- .item -->
					</div><!-- .col -->
					
				</div><!-- .grid -->

		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>