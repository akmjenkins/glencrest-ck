<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/images/temp/hero/hero-2.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="small-wrap center">
				<div class="hgroup">
					<h1 class="hgroup-title">Phases</h1>
				</div><!-- .hgroup -->
				
				<p>
					St. John’s and the Avalon Peninsula are continuing to experience incredible growth. As the major service centre for the province’s burgeoning oil and gas sector and other major industries, the city is positioned for expansion far into the future. Glencrest is coming on stream at precisely the opportune time as businesses look for new and expansion opportunities.
				</p>
				
			</div><!-- .small-wrap -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="msp-section">
	
		
		<div class="tab-wrapper">
			
			<div class="tab-controls">
			
				<div class="msp-selector">
					<div class="selector with-arrow">
						<select  class="tab-controller">
							<option selected>Phase 1</option>
							<option>Phase 2</option>
							<option>Phase 3</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
				</div><!-- .masp-selector -->
			
			</div><!-- .tab-controls -->
			
			<div class="tab-holder msp-tabs">
			
				<div class="tab selected">
			
					<div class="tab-wrapper">
					
						<div class="tab-controls">
						
							<div class="sw full">
							
								<div class="msp-controls">
									<button class="t-fa fa-map-marker tab-control selected">
										<span>Map</span>
									</button>
									<button class="t-fa fa-bar-chart tab-control">
										<span>Status</span>
									</button>
									<button class="t-fa fa-camera tab-control">
										<span>Photos</span>
									</button>
								</div><!-- .msp-controls -->
								
							</div><!-- .sw -->
							
						</div><!-- .tab-controls -->
						<div class="tab-holder">
						
							<div class="tab selected msp-map-tab">
								
								<div class="gmap msp-map">
									<div data-center="47.5012708,-52.8251804" data-zoom="15">&nbsp;</div>
								</div><!-- .gmap -->
								
							</div><!-- .,tab -->
							
							<div class="tab msp-status-tab">
								
								<div class="small-wrap pad-wrap">
									
									<div class="filter-section">
										<div class="filter-bar">

											<div class="filter-bar-left">
											
												<div class="count">
													<strong>10</strong> Updates
												</div><!-- .count -->
												
											</div><!-- .filter-bar-left -->

											<div class="filter-bar-meta">
												
												<div class="filter-controls">
													<button class="previous">Prev</button>
													<button class="next">Next</button>
												</div><!-- .filter-controls -->
											
											</div><!-- .filter-bar-meta -->
												
										</div><!-- .filter-bar -->
										<div class="filter-content">
										
											<div class="grid eqh msp-grid">
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->

												
												
											</div><!-- .msp-grid -->

										
										</div><!-- .filter-content -->
									</div><!-- .filter-section -->
									
								</div><!-- .small-wrap -->
								
							</div><!-- .tab -->
							
							<div class="tab msp-photos-tab">
							
								<!-- large thumbnail swiper -->
								<div class="sw pad-wrap">
									<div class="msp-lg-images">
										<div class="swiper-wrapper-lg-images">
											<div class="swiper">
												<a href="../assets/images/temp/gallery/gallery-big.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-big.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-2.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-2.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-3.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-3.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-4.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-4.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-5.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-5.jpg">
													<span></span>
												</a>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .lg-images -->
								</div><!-- .sw -->
								
								<!-- nav swiper -->
								<div class="msp-sm-images">
									<div class="sw">
										<div class="swiper-wrapper-sm-images">
											<div class="swiper">
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-1.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-2.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-3.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-4.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-5.jpg)"></div>
												</div>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .sw -->
								</div><!-- .sm-images -->
							
							</div><!-- .tab -->
							
						</div><!-- .tab-holder -->
					</div><!-- .tab-wrapper -->

				</div><!-- .tab -->

				<div class="tab">
			
					<div class="tab-wrapper">
					
						<div class="tab-controls">
						
							<div class="sw full">
							
								<div class="msp-controls">
									<button class="t-fa fa-map-marker tab-control selected">
										<span>Map</span>
									</button>
									<button class="t-fa fa-bar-chart tab-control">
										<span>Status</span>
									</button>
									<button class="t-fa fa-camera tab-control">
										<span>Photos</span>
									</button>
								</div><!-- .msp-controls -->
								
							</div><!-- .sw -->
							
						</div><!-- .tab-controls -->
						<div class="tab-holder">
						
							<div class="tab selected msp-map-tab">
								
								<div class="gmap msp-map">
									<div data-center="47.5012708,-52.8251804" data-zoom="15">&nbsp;</div>
								</div><!-- .gmap -->
								
							</div><!-- .,tab -->
							
							<div class="tab msp-status-tab">
								
								<div class="small-wrap pad-wrap">
									
									<div class="filter-section">
										<div class="filter-bar">

											<div class="filter-bar-left">
											
												<div class="count">
													<strong>10</strong> Updates
												</div><!-- .count -->
												
											</div><!-- .filter-bar-left -->

											<div class="filter-bar-meta">
												
												<div class="filter-controls">
													<button class="previous">Prev</button>
													<button class="next">Next</button>
												</div><!-- .filter-controls -->
											
											</div><!-- .filter-bar-meta -->
												
										</div><!-- .filter-bar -->
										<div class="filter-content">
										
											<div class="grid eqh msp-grid">
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->

												
												
											</div><!-- .msp-grid -->

										
										</div><!-- .filter-content -->
									</div><!-- .filter-section -->
									
								</div><!-- .small-wrap -->
								
							</div><!-- .tab -->
							
							<div class="tab msp-photos-tab">
							
								<!-- large thumbnail swiper -->
								<div class="sw pad-wrap">
									<div class="msp-lg-images">
										<div class="swiper-wrapper-lg-images">
											<div class="swiper">
												<a href="../assets/images/temp/gallery/gallery-big.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-big.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-2.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-2.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-3.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-3.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-4.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-4.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-5.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-5.jpg">
													<span></span>
												</a>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .lg-images -->
								</div><!-- .sw -->
								
								<!-- nav swiper -->
								<div class="msp-sm-images">
									<div class="sw">
										<div class="swiper-wrapper-sm-images">
											<div class="swiper">
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-1.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-2.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-3.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-4.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-5.jpg)"></div>
												</div>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .sw -->
								</div><!-- .sm-images -->
							
							</div><!-- .tab -->
							
						</div><!-- .tab-holder -->
					</div><!-- .tab-wrapper -->

				</div><!-- .tab -->

				<div class="tab">
			
					<div class="tab-wrapper">
					
						<div class="tab-controls">
						
							<div class="sw full">
							
								<div class="msp-controls">
									<button class="t-fa fa-map-marker tab-control selected">
										<span>Map</span>
									</button>
									<button class="t-fa fa-bar-chart tab-control">
										<span>Status</span>
									</button>
									<button class="t-fa fa-camera tab-control">
										<span>Photos</span>
									</button>
								</div><!-- .msp-controls -->
								
							</div><!-- .sw -->
							
						</div><!-- .tab-controls -->
						<div class="tab-holder">
						
							<div class="tab selected msp-map-tab">
								
								<div class="gmap msp-map">
									<div data-center="47.5012708,-52.8251804" data-zoom="15">&nbsp;</div>
								</div><!-- .gmap -->
								
							</div><!-- .,tab -->
							
							<div class="tab msp-status-tab">
								
								<div class="small-wrap pad-wrap">
									
									<div class="filter-section">
										<div class="filter-bar">

											<div class="filter-bar-left">
											
												<div class="count">
													<strong>10</strong> Updates
												</div><!-- .count -->
												
											</div><!-- .filter-bar-left -->

											<div class="filter-bar-meta">
												
												<div class="filter-controls">
													<button class="previous">Prev</button>
													<button class="next">Next</button>
												</div><!-- .filter-controls -->
											
											</div><!-- .filter-bar-meta -->
												
										</div><!-- .filter-bar -->
										<div class="filter-content">
										
											<div class="grid eqh msp-grid">
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->
												
												<div class="col">
													<div class="item">
													
														<div class="hgroup">
															<span class="hgroup-title">Page Title</span>
															<time class="hgroup-subtitle">Feb 10, 2015</time>
														</div><!-- .hgroup -->
														
														<p>
															Etiam ut quam non eros pharetra tristique. Cras lobortis auctor mollis. Suspendisse magna lacus, imperdiet et venenatis luctus, mollis in justo. Sed lacinia fringilla vehicula.
														</p>
													
														<a href="#" class="button outline">Read More</a>
													
													</div><!-- .item -->
												</div><!-- .col -->

												
												
											</div><!-- .msp-grid -->

										
										</div><!-- .filter-content -->
									</div><!-- .filter-section -->
									
								</div><!-- .small-wrap -->
								
							</div><!-- .tab -->
							
							<div class="tab msp-photos-tab">
							
								<!-- large thumbnail swiper -->
								<div class="sw pad-wrap">
									<div class="msp-lg-images">
										<div class="swiper-wrapper-lg-images">
											<div class="swiper">
												<a href="../assets/images/temp/gallery/gallery-big.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-big.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-2.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-2.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-3.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-3.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-4.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-4.jpg">
													<span></span>
												</a>
												<a href="../assets/images/temp/gallery/gallery-thumb-5.jpg" data-gallery="image-gallery-1" class="swipe-item mpopup" data-src="../assets/images/temp/gallery/gallery-thumb-5.jpg">
													<span></span>
												</a>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .lg-images -->
								</div><!-- .sw -->
								
								<!-- nav swiper -->
								<div class="msp-sm-images">
									<div class="sw">
										<div class="swiper-wrapper-sm-images">
											<div class="swiper">
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-1.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-2.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-3.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-4.jpg)"></div>
												</div>
												<div class="swipe-item">
													<div style="background-image: url(../assets/images/temp/gallery/gallery-thumb-5.jpg)"></div>
												</div>
											</div><!-- .swiper -->
										</div><!-- .swiper-wrapper -->
									</div><!-- .sw -->
								</div><!-- .sm-images -->
							
							</div><!-- .tab -->
							
						</div><!-- .tab-holder -->
					</div><!-- .tab-wrapper -->

				</div><!-- .tab -->
				
			</div><!-- .tab-holder -->
			
		</div><!-- .tab-wrapper -->
	
	
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>