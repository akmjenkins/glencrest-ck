<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" data-src="
			../assets/images/temp/hero/hero-1.jpg,
			http://dummyimage.com/1200x500/000/fff 1200w,
			http://dummyimage.com/600x500/000/fff 600w,
		">
		
			<a href="//player.vimeo.com/video/105192180?byline=0&portrait=0&autoplay=1" class="mpopup video-link">
				<span class="watch t-fa fa-play">Watch Now</span>
			</a><!-- .video-link -->
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hero-caption-text">
							<span class="title h2-style">Atlantic Canada's newest industrial park.</span>
							<span class="subtitle">103 acres of prime real estate strategically located and ready for sale.</span>
						</div><!-- .hero-caption-text -->
						
						<a href="#" class="button">Sales Inquiries</a>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="swiper-wrapper">
				<div class="swiper">
					<div class="swipe-item">
					
						<div class="grid ov-grid">
							<div class="col">
								<a class="item ov-item" href="#">
									
									<div class="img-time ar" data-ar="45">
										<div class="ar-child lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
										
										<time>
											<span class="t-wrap">
												Nov 23 <span class="year">2015</span>
											</span><!-- .t-wrap -->
										</time>
										
									</div><!-- .img-time -->
									
									<p>
										Lorem ipsum dolor sit amet, consect elit. Sed a diam turpis. Duis venenatistes nib imperdiet condimentum felis nec.
									</p>
									
								</a><!-- .ov-item -->
							</div><!-- .col -->
							<div class="col">
								<a class="item ov-item" href="#">
									
									<div class="img-time ar" data-ar="45">
										<div class="ar-child lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
										
										<time>
											<span class="t-wrap">
												Nov 23 <span class="year">2015</span>
											</span><!-- .t-wrap -->
										</time>
										
									</div><!-- .img-time -->
									
									<p>
										Lorem ipsum dolor sit amet, consect elit. Sed a diam turpis. Duis venenatistes nib imperdiet condimentum felis nec.
									</p>
									
								</a><!-- .ov-item -->
							</div><!-- .col -->
							<div class="col">
								<a class="item ov-item" href="#">
									
									<div class="img-time ar" data-ar="45">
										<div class="ar-child lazybg" data-src="../assets/images/temp/block-1.jpg"></div>
										
										<time>
											<span class="t-wrap">
												Nov 23 <span class="year">2015</span>
											</span><!-- .t-wrap -->
										</time>
										
									</div><!-- .img-time -->
									
									<p>
										Lorem ipsum dolor sit amet, consect elit. Sed a diam turpis. Duis venenatistes nib imperdiet condimentum felis nec.
									</p>
									
								</a><!-- .ov-item -->
							</div><!-- .col -->
						</div><!-- .grid -->
					
					</div><!-- .swipe-item -->
				</div><!-- .swiper -->
			</div><!-- .swiper-wrapper -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>