<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item dark-bg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hgroup">
							<h1 class="hgroup-title">Status Post Single</h1>
							<time class="hgroup-subtitle" pubdate datetime="2015-02-10">February 10, 2015</time>
						</div><!-- .hgroup -->
					
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet tellus elit. Ut eget mollis lacus. Donec molestie nibh eu nulla tempor porta. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices condimentum pellentesque. 
						</p>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content">
				
					<div class="article-body">
					
						<p>
							Phasellus quis finibus augue, nec venenatis metus. Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper. 
							Aliquam non elementum elit, nec ultricies turpis. Ut cursus tempus augue. Morbi consectetur justo sit amet est dictum, quis consectetur 
							nunc ornare. Proin cursus lacinia aliquam. Donec rutrum sodales mattis. Nunc quis fringilla mauris, at interdum augue. 
							Phasellus sed aliquam lectus, ut rutrum quam. Aenean congue magna et sapien venenatis, at laoreet nisl porta. Nam porta vestibulum 
							pellentesque. Donec id tristique massa, at lacinia lectus.
						</p>
						
						<h2>Header 2 - H2</h2>
						<h3>Header 3 - H3</h3>
						<h4>Header 4 - H4</h4>
						<h5>Header 5 - H5</h5>
						<h6>Header 6 - H6</h6>

						<p>
							Nullam cursus, dui eget imperdiet dapibus, leo dui pretium libero, non facilisis massa felis et lacus. Suspendisse rutrum euismod turpis 
							vitae commodo. Sed in ante vel felis rutrum iaculis eget vitae ipsum. Praesent sollicitudin eros eu orci elementum porttitor. Aliquam efficitur 
							imperdiet volutpat. Pellentesque eget vestibulum dolor. Nunc sit amet pulvinar justo.
						</p>
						
						<h3>Links</h3>
						<p>
						
							<em>
								Note: styles apply to links in paragraphs/lists/blockquotes, or <code>&lt;a&gt;</code> tags with a class of <code>.inline</code>
							</em>
							
							<br />
							<br />
						
							<a href="#link">Link</a>
							<a href="#link" class="hover">Link:hover</a>
							<a href="#link" class="visited">Link:visited</a>
							<a href="#link" class="active">Link:active</a>
							
							<br />
							<br />
							
							And this is what a link will <a href="#link">look like</a> when it is written in a <a href="#link">regular paragraph</a>.
							
						</p>
						
						<hr />
						
						<ul>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ul>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ul>
							</li>
						</ul>
						
						<ol>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
							<li>
								Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.
								<ol>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>
									<li>Etiam eget consequat felis. Etiam nec magna ut libero vulputate ullamcorper.</li>								
								</ol>
							</li>
						</ol>
					
					</div><!-- .article-body -->
					
				</div><!-- .content -->
				
				<aside class="sidebar">
					
					<div class="archives-mod mod">
						<h5 class="mod-title">Archives</h5>
						
						<div class="acc with-indicators inv">
						
							<div class="acc-item">
								<div class="acc-item-handle">
									2014 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									2013 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									2012 (11)
								</div><!-- .acc-item-handle -->
								<div class="acc-item-content">
									<ul>
										<li><a href="#">October (2)</a></li>
										<li><a href="#">September (5)</a></li>
										<li><a href="#">August (6)</a></li>
									</ul>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

							
						</div><!-- .acc -->
					</div><!-- .archives-mod -->

				</aside><!-- .sidebar -->
				
			</div><!-- .main-body -->
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>