<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap with-content">

	<div class="big-fader fader">
		<div class="fader-item dark-bg">
		
			<div class="hero-caption">
				<div class="sw">
					
					<div class="hero-caption-content">
					
						<div class="hgroup">
							<h1 class="hgroup-title">Benefits</h1>
						</div><!-- .hgroup -->
					
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce laoreet tellus elit. Ut eget mollis lacus. Donec molestie nibh eu nulla tempor porta. 
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices condimentum pellentesque. 
						</p>
						
					</div><!-- .hero-caption-content -->
					
				</div><!-- .sw -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content">
					
					<div class="grid fill eqh">
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg dark-blue-bg pad-20 center">
							
								<div class="hgroup">
									<h4 class="hgroup-title">Benefit One</h4>
								</div><!-- .hgroup -->
							
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit 
								</p>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg dark-blue-bg pad-20 center">
							
								<div class="hgroup">
									<h4 class="hgroup-title">Benefit One</h4>
								</div><!-- .hgroup -->
							
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit 
									amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
								</p>
							
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col col-3 sm-col-2 xs-col-1">
							<div class="item dark-bg dark-blue-bg pad-20 center">
							
								<div class="hgroup">
									<h4 class="hgroup-title">Benefit One</h4>
								</div><!-- .hgroup -->
							
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit 
									amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
								</p>
							
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
					
					<blockquote class="embellished-blockquote">
						Aenean ut sapien quis est ultricies dignissim. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris in orci vitae erat 
						consectetur eleifend imperdiet eget nunc. Proin sit amet tempus lacus. Curabitur a volutpat augue. Donec eu nisi ut nisl blandit feugiat in a eros. 
						Praesent ac purus id ligula finibus luctus. Morbi hendrerit semper neque, ut finibus mauris suscipit non. 
					</blockquote><!-- .embellished -->
					
				</div><!-- .content -->
				
			</div><!-- .main-body -->		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>